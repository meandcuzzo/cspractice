﻿// This is a using statement.
using System;
using System.IO;

namespace cspractice
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string item in args)
            {
                Console.WriteLine("ARGS ARE " + item);
            }
            Console.WriteLine(TestFunc(4, 5));
        }
        static int TestFunc(int x, int y)
        {
            return x + y;
        }
    }
}
